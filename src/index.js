import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
// Загружаемые страницы
import First from './First';
import Second from './Second';
import Third from './Third';

import {BrowserRouter, Route,Routes} from 'react-router-dom'; //Компоненты react-router-dom
import {ThemeContext} from './contexts/contexts'; //Созданный контекст
import {createStore} from 'redux';  //Метод Redux для создания хранилищ

//Работа с Redux
//  Начальное состояние
const initialState = {
  name: "danya",
  mood: 'sad'
};

// Действия, меняющие состояния после отправки
const becomeHappy = {
  type: 'BECOME_HAPPY'
};

const becomeSad = {
  type: 'BECOME_SAD'
};

/*
* Редусер, кототрый интерпритирует состояние
* @param: state - текущее состояние
* @param: action - действие
* @return: измененный state
*/
function reducer(state, action) {
  switch (action.type) {
    case 'BECOME_HAPPY': return {...state, mood: 'happy'};
      break;
    case 'BECOME_SAD': return {...state, mood: 'sad'};
      break;
    default: return state;
  }
}

/*
* Функия, вызывающаяся после отправки каждого действия
* @return: текущее состояние
*/
const stateListener = () => {
  const currentState = store.getState();
  console.log(currentState);
}

/*
*  createStore - Создание хранилища
*  @param: reducer - Редусер состояния
*  @param: initialState - состояние
*/
const store = createStore(reducer, initialState);
// Попдись хранилища на функцию stateListener
store.subscribe(stateListener);
console.log('Текущее состояние');
console.log(store.getState());
// Отправка действии
console.log('Отправка действия');
store.dispatch(becomeHappy);
console.log('Еще одно отправленное действие');
store.dispatch(becomeSad);











class Project extends React.Component {
  constructor() {
    super();
    this.state = {color: 'blue'};
    this.changeContextValue = this.changeContextValue.bind(this);
  }

  changeContextValue = () => {
    this.setState(prevState => {
      return {color: prevState.color === 'blue' ? 'orange': 'blue'}
    });
  }

  render() {
    return(
        <BrowserRouter>
          <ThemeContext.Provider value={{theme: this.state.color,  changeContextValue: this.changeContextValue}}>
            <Routes>
              <Route exact path="/" element={<App/>}></Route>
              <Route exact path="/first" element={<First/>}></Route>
              <Route exact path="/second" element={<Second/>}></Route>
              <Route exact path="/third" element={<Third/>}></Route>
            </Routes>
          </ThemeContext.Provider>
        </BrowserRouter>
    )
  }
}

ReactDOM.render( <Project/>, document.getElementById('root'));

import React from 'react';
import {ThemeContext} from './contexts/contexts';


class Second extends React.Component {

  render() {
    console.log(this.context);
    return(
      <div style={{backgroundColor: this.context.theme}}>
        <h2>Second page</h2>
        <button onClick={this.context.changeContextValue}>change</button>
      </div>
    );
  }
}

Second.contextType = ThemeContext;
export default Second;

import React from 'react';

function App() {
  return(
      <div>
        <a href="/first"><p>Переход к первой странице</p></a>
        <a href="/second"><p>Переход к второй странице</p></a>
        <a href="/third"><p>Переход к третей странице</p></a>
      </div>
  )
}

export default App;

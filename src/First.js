import React, {useEffect, useState} from 'react';
import {ThemeContext} from './contexts/contexts';

function First() {

  const [state, setState] = useState(0)
  useEffect(() => {
    console.log('Примонтировался')}, []);
  useEffect(() => {
    return () => {console.log('Компонент отмонтировался');}
  })

  return(
    <ThemeContext.Consumer>
      {theme =>
        <div style={{backgroundColor: theme.theme}}>
          <h2 onClick={() => setState(state + 1)}>First page {state}</h2>
          <button onClick={theme.changeContextValue}>Change</button>
        </div>
      }
    </ThemeContext.Consumer>
  )
}

export default First;
